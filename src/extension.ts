/**
 * Eastwood for Visual Studio Code
 * Copyright (C) 2022  TOP Software Technology B.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
	ExtensionContext,
	OutputChannel,
	TextDocument,
	Uri,
	window as Window,
	workspace as Workspace,
	WorkspaceFolder,
	WorkspaceFoldersChangeEvent
} from 'vscode';
import {
	LanguageClient,
	LanguageClientOptions,
	TransportKind
} from 'vscode-languageclient/node';

// We have one client for each workspace.
let clients: Map<string, LanguageClient> = new Map();

// Cached list of the folders of the workspace. Access with `sortedWorkspaceFolders()`.
let _sortedWorkspaceFolders: string[] | undefined;
function sortedWorkspaceFolders(): string[] {
	if (_sortedWorkspaceFolders === void 0) {
		_sortedWorkspaceFolders = Workspace.workspaceFolders ? Workspace.workspaceFolders.map(folder => {
			let result = folder.uri.toString();
			if (result.charAt(result.length - 1) !== '/') {
				result = result + '/';
			}
			return result;
		}).sort((a, b) => a.length - b.length) : [];
	}
	return _sortedWorkspaceFolders;
}
Workspace.onDidChangeWorkspaceFolders(() => _sortedWorkspaceFolders = undefined);

function getOuterMostWorkspaceFolder(folder: WorkspaceFolder): WorkspaceFolder {
	for (let element of sortedWorkspaceFolders()) {
		let uri = folder.uri.toString();
		if (uri.charAt(uri.length - 1) !== '/') {
			uri = uri + '/';
		}
		if (uri.startsWith(element)) {
			return Workspace.getWorkspaceFolder(Uri.parse(element))!;
		}
	}
	return folder;
}

export function activate(_: ExtensionContext): void {
	let command = 'nitrile';
	let outputChannel: OutputChannel = Window.createOutputChannel('eastwood-cls');

	function didOpenTextDocument(document: TextDocument): void {
		// We are only interested in Clean code. We do not handle untitled files (document.uri.scheme ===  'untitled').
		if (document.languageId !== 'clean' || document.uri.scheme !== 'file') {
			return;
		}

		let folder = Workspace.getWorkspaceFolder(document.uri);
		// Files outside a folder can't be handled.
		if (!folder) {
			return;
		}

		// If we have nested workspace folders we only start a server on the outermost workspace folder.
		folder = getOuterMostWorkspaceFolder(folder);
		let args = ['run', 'eastwood', 'eastwood-cls'];

		if (!clients.has(folder.uri.toString())) {
			let serverOptions = {
				run: { command: command, transport: TransportKind.stdio, args: args},
				debug: { command: command, transport: TransportKind.stdio, args: args}
			};

			let clientOptions: LanguageClientOptions = {
				documentSelector: [
					{ scheme: 'file', language: 'clean', pattern: `${folder.uri.fsPath}/**/*` }
				],
				diagnosticCollectionName: 'eastwood-cls',
				workspaceFolder: folder,
				outputChannel: outputChannel
			};
			let client = new LanguageClient('eastwood-cls', 'Eastwood Language Server', serverOptions, clientOptions);
			client.start();
			clients.set(folder.uri.toString(), client);
		}
	}

	function didChangeWorkspaceFolders(event: WorkspaceFoldersChangeEvent): void {
		for (let folder of event.removed) {
			let client = clients.get(folder.uri.toString());
			if (client) {
				clients.delete(folder.uri.toString());
				client.stop();
			}
		}
	}

	Workspace.onDidOpenTextDocument(didOpenTextDocument);
	Workspace.textDocuments.forEach(didOpenTextDocument);
	Workspace.onDidChangeWorkspaceFolders(didChangeWorkspaceFolders);
}

export function deactivate(): Thenable<void> {
	return Array.from(clients.values()).reduce((promise, client) =>
		promise.then(() => client.stop()),
		Promise.resolve()
	);
}
