#! /bin/bash

set -e

apt-get update -qq
apt-get upgrade -qq
apt-get install -qq --no-install-recommends libsecret-1-0
npm install
npm install -g vsce
npm install -g npm-check-updates
