# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

#### [0.1.3] 2024-07-09

- Chore: update several dependencies.

#### [0.1.2] 2023-12-04

- Chore: update several dependencies.

#### [0.1.1] 2022-11-02

- Fix: arguments are now correctly passed to nitrile for starting the language server.

#### [0.1.0] 2022-11-01

- Change: extension now depends on [nitrile](https://clean-lang.org/about.html#install). This is a breaking change.

#### [0.0.2] - 2022-03-29

- Update README reflecting recommended way of usage (within devcontainer).

#### [0.0.1] - 2022-03-25

- First published release.

[development]: https://gitlab.com/top-software/clean-vs-code/-/commits/main
[0.2.0]:https://gitlab.com/top-software/clean-vs-code/-/tags/0.2.0
[0.0.2]:https://gitlab.com/top-software/clean-vs-code/-/tags/0.0.2
[0.0.1]:https://gitlab.com/top-software/clean-vs-code/-/tags/0.0.1
