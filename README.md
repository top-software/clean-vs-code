# Clean for Visual Studio Code

This extension provides support for the functional programming language
[Clean][] in [Visual Studio Code][]. It currently provides the following
features:

- Syntax highlighting
- Integration of [Eastwood][], the language server for Clean

## Requirements

This extension requires `eastwood-cls` to be installed using [nitrile](https://clean-lang.org/).

After installing [nitrile itself](https://clean-lang.org/about.html#install),
You can install it using the `nitrile global install eastwood` command.

The aforementioned command ensures `eastwood-cls` is on your path.

When using a devcontainer for your project the installation can be done in the installation phase of the devcontainer.

## Installation instructions (for usage)

The package is available on the [VS Code Extension Marketplace](https://marketplace.visualstudio.com/vscode):
[Clean for Visual Studio Code](https://marketplace.visualstudio.com/manage/publishers/topsoftware/extensions/clean-vs-code/hub).
The recommended way of developing Clean programs is using a devcontainer.
A [template project](https://gitlab.com/top-software/itasks-template) is provided including an iTasks project
with minimal configuration.

## Installation instructions (for development)

This requires TypeScript and VSC to be installed. On Debian the former can be
installed with:

```
apt install npm
npm install -g typescript
```

For the latter [installation instructions](https://wiki.debian.org/VisualStudioCode)
are available on the Debian Wiki.

To run the extension do:

```bash
git clone https://gitlab.com/top-software/clean-vs-code.git
cd clean-vs-code
npm install
code .
```

In the editor that opens, press F5 to compile and run the extension in a new
Extension Development Host window. In that window, you can run the "Developer:
Reload Window" command (from the command palette, Ctrl-Shift-P) to reload the
extension after you have made changes.

The following links may be helpful if you're new to VS Code extensions:

- https://code.visualstudio.com/api/get-started/your-first-extension
- https://code.visualstudio.com/api/language-extensions/language-server-extension-guide

## Usage

Diagnostics for Clean source files can only be provided if a directory with a Clean
project file `Eastwood.yml` is opened.

## Release Notes

See the [Change Log](/CHANGELOG.md).

## Copyright &amp; License

Eastwood for Visual Studio Code is copyright &copy; 2021 TOP Software Technology B.V.

This extension is licensed under GPL v3.0. For details, see the
[LICENSE](/LICENSE) file.

An exception exists for `syntaxes/clean.tmLanguage.json` which originates from the
[CleanForVSCode](https://github.com/W95Psp/CleanForVSCode) and is therefore licensed under the same copyright and the
same license of that project (MIT License, Copyright (c) 2021 Lucas Franceschino).

[Clean]: http://clean.cs.ru.nl/
[Eastwood]: https://gitlab.com/top-software/eastwood/
[Visual Studio Code]: https://code.visualstudio.com/
